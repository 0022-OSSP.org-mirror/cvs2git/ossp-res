/*
**  OSSP res - Resource Pools
**  Copyright (c) 2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2003 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP res, a resource pool library
**  which can be found at http://www.ossp.org/pkg/lib/res/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  res.h: resource pools
*/

#ifndef __RES_H__
#define __RES_H__

struct res_st;
typedef struct res_st res_t;

res_rc_t res_create   (res_t **res, res_t *parent);
res_rc_t res_register (res_t  *res, res_item_t item, ...);
res_rc_t res_insert   (res_t  *res, res_item_t item, ...);
res_rc_t res_remove   (res_t  *res, res_item_t item, ...);
res_rc_t res_destroy  (res_t  *res);

#endif /* __RES_H__ */

