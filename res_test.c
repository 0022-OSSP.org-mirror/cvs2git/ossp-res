/*
**  OSSP res - Resource Pools
**  Copyright (c) 2003 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2003 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP res, a resource pool library
**  which can be found at http://www.ossp.org/pkg/lib/res/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  res_test.c: resource pools test suite
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "ts.h"
#include "res.h"

TS_TEST(test_foo)
{
    ts_test_check(TS_CTX, "foo");

#if 0
    ts_test_fail(TS_CTX, "v1 = %d (!= 5678)", 1);
#endif
}

int main(int argc, char *argv[])
{
    ts_suite_t *ts;
    int n;

    ts = ts_suite_new("OSSP ex (Exception Handling)");
    ts_suite_test(ts, test_foo, "foo");
    n = ts_suite_run(ts);
    ts_suite_free(ts);
    return n;
}

