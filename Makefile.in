##
##  OSSP res - Resource Pools
##  Copyright (c) 2003 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2003 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP res, a resource pool library
##  which can be found at http://www.ossp.org/pkg/lib/res/.
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  Makefile.in: make(1) build procedure
##

@SET_MAKE@

DESTDIR     =
prefix      = @prefix@
exec_prefix = @exec_prefix@
bindir      = @bindir@
libdir      = @libdir@
includedir  = @includedir@
mandir      = @mandir@

CC          = @CC@
CPPFLAGS    = @CPPFLAGS@
CFLAGS      = @DEFS@ @CFLAGS@
LDFLAGS     = @LDFLAGS@
LIBS        = @LIBS@
RM          = rm -f
RMDIR       = rmdir
SHTOOL      = ./shtool
LIBTOOL     = ./libtool
TRUE        = true
POD2MAN     = pod2man

LIB_NAME    = libres.la
LIB_OBJS    = res.lo

TST_NAME    = res_test
TST_OBJS    = res_test.o ts.o

.SUFFIXES:
.SUFFIXES: .c .o .lo

all: $(LIB_NAME) $(TST_NAME)

.c.o:
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $<

.c.lo:
	@$(LIBTOOL) --mode=compile $(CC) $(CPPFLAGS) $(CFLAGS) -c $<

$(LIB_OBJS): Makefile
$(TST_OBJS): Makefile

$(LIB_NAME): $(LIB_OBJS)
	@$(LIBTOOL) --mode=link $(CC) -o $(LIB_NAME) $(LIB_OBJS) -rpath $(libdir) \
	    -version-info `$(SHTOOL) version -l txt -d libtool VERSION`

$(TST_NAME): $(TST_OBJS) $(LIB_NAME)
	@$(LIBTOOL) --mode=link $(CC) $(LDFLAGS) -o $(TST_NAME) $(TST_OBJS) $(LIB_NAME) $(LIBS)

man: res.3
res.3: res.pod
	V1=`$(SHTOOL) version -l txt -d short VERSION`; \
	V2=`$(SHTOOL) version -l txt -d long VERSION`; \
	D=`$(SHTOOL) version -l txt -d long VERSION | sed -e 's;.*(;;' -e 's;).*;;'`; \
	$(POD2MAN) --quotes=none \
	           --section=3 --center="Resource Pools" \
	           --release="$$D" --date="OSSP res $$V1" res.pod | \
	sed -e "s;RES_VERSION_STR;$$V2;" >res.3

check: $(TST_NAME)
	@$(LIBTOOL) --mode=execute ./$(TST_NAME)

install:
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(prefix)
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(bindir)
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(includedir)
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(libdir)
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(mandir)/man3
	$(SHTOOL) install -c -m 755 res-config $(DESTDIR)$(bindir)/
	$(SHTOOL) install -c -m 644 res.h $(DESTDIR)$(includedir)/
	$(SHTOOL) install -c -m 644 res.3 $(DESTDIR)$(mandir)/man3/
	@$(LIBTOOL) --mode=install $(SHTOOL) install -c -m 644 libres.la $(DESTDIR)$(libdir)/

uninstall:
	@$(LIBTOOL) --mode=uninstall $(RM) $(DESTDIR)$(libdir)/libres.la
	-$(RM) $(DESTDIR)$(mandir)/man3/res.3
	-$(RM) $(DESTDIR)$(includedir)/res.h
	-$(RM) $(DESTDIR)$(bindir)/res-config
	-$(RMDIR) $(DESTDIR)$(mandir)/man3 >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(mandir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(libdir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(includedir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(bindir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(prefix) >/dev/null 2>&1 || $(TRUE)

clean:
	-$(RM) $(LIB_NAME) $(LIB_OBJS)
	-$(RM) $(TST_NAME) $(TST_OBJS)
	-$(RM) -r .libs >/dev/null 2>&1 || $(TRUE)
	-$(RM) *.o *.lo

distclean: clean
	-$(RM) config.log config.status config.cache
	-$(RM) Makefile config.h res-config
	-$(RM) libtool

realclean: distclean
	-$(RM) res.3
	-$(RM) configure config.h.in
	-$(RM) shtool
	-$(RM) ltmain.sh libtool.m4 config.guess config.sub

