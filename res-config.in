#!/bin/sh
##
##  OSSP res - Resource Pools
##  Copyright (c) 2003 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2003 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP res, a resource pool library
##  which can be found at http://www.ossp.org/pkg/lib/res/.
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  res-config.in: library build utility
##

DIFS=' 
'

#   tool
tool_name="res-config"

#   library version
lib_name="OSSP res"
lib_version="@RES_VERSION_STR@"

#   build paths
prefix="@prefix@"
exec_prefix="@exec_prefix@"
bindir="@bindir@"
libdir="@libdir@"
includedir="@includedir@"
mandir="@mandir@"
datadir="@datadir@"

#   build options
cflags="@CFLAGS@"
ldflags="@LDFLAGS@"
libs="@LIBS@"

#   option defaults
help=no
version=no

usage="$tool_name"
usage="$usage [--help] [--version] [--all]"
usage="$usage [--prefix] [--exec-prefix] [--bindir] [--libdir] [--includedir] [--mandir] [--datadir]"
usage="$usage [--cflags] [--ldflags] [--libs]"
if [ $# -eq 0 ]; then
    echo "$tool_name:Error: Invalid option" 1>&2
    echo "$tool_name:Usage: $usage" 1>&2
    exit 1
fi
output=''
output_extra=''
all=no
prev=''
OIFS="$IFS" IFS="$DIFS"
for option
do
    if [ ".$prev" != . ]; then
        eval "$prev=\$option"
        prev=''
        continue
    fi
    case "$option" in
        -*=*) optarg=`echo "$option" | sed 's/[-_a-zA-Z0-9]*=//'` ;;
           *) optarg='' ;;
    esac
    case "$option" in
        --help|-h)
            echo "Usage: $usage"
            exit 0
            ;;
        --version|-v)
            echo "$lib_name $lib_version"
            exit 0
            ;;
        --all)
            all=yes
            ;;
        --prefix)
            output="$output $prefix"
            ;;
        --exec-prefix)
            output="$output $exec_prefix"
            ;;
        --bindir)
            output="$output $bindir"
            ;;
        --libdir)
            output="$output $libdir"
            ;;
        --includedir)
            output="$output $includedir"
            ;;
        --mandir)
            output="$output $mandir"
            ;;
        --datadir)
            output="$output $datadir"
            ;;
        --cflags)
            output="$output -I$includedir"
            output_extra="$output_extra $cflags"
            ;;
        --ldflags)
            output="$output -L$libdir"
            output_extra="$output_extra $ldflags"
            ;;
        --libs)
            output="$output -lres"
            output_extra="$output_extra $libs"
            ;;
        * )
            echo "$tool_name:Error: Invalid option" 1>&2
            echo "$tool_name:Usage: $usage" 1>&2
            exit 1;
            ;;
    esac
done
IFS="$OIFS"
if [ ".$prev" != . ]; then
    echo "$tool_name:Error: missing argument to --`echo $prev | sed 's/_/-/g'`" 1>&2
    exit 1
fi
if [ ".$output" != . ]; then
    if [ ".$all" = .yes ]; then
        output="$output $output_extra"
    fi
    echo $output
fi

